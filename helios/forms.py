# -*- coding: utf-8 -*-
"""
Forms for Helios
"""

from django import forms
from models import Election
from widgets import *
from fields import *
from django.conf import settings


class ElectionForm(forms.Form):
  short_name = forms.SlugField(max_length=40, label='Identificador', help_text='Não pode conter espaços em branco, acentos, cedilhas e caracteres especiais (exceto - ou _).<br>Exemplo: Eleicao-2018 ou Eleicao_2018.')
  name = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'size':60}), label='Nome', help_text='Nome da sua votação (pode conter espaços em branco, acentos e cedilhas).<br>Exemplo: Eleição 2016')
  description = forms.CharField(max_length=4000, widget=forms.Textarea(attrs={'cols': 70, 'wrap': 'soft'}), required=False, label='Descrição')
  election_type = forms.ChoiceField(label="Tipo", choices = Election.ELECTION_TYPES)
  use_voter_aliases = forms.BooleanField(required=False, initial=False, label='Uso de apelidos?', help_text='Se selecionado, os participantes serão identificados por apelidos no Centro de Assinatura de Cédulas. Exemplo "V12".')
  #use_advanced_audit_features = forms.BooleanField(required=False, initial=True, help_text='disable this only if you want a simple election with reduced security but a simpler user interface')
  randomize_answer_order = forms.BooleanField(required=False, initial=False, label='Ordem aleatória das opções?', help_text='Se selecionado, as opções de cada pergunta aparecem de forma aleatória para cada participante.')
  private_p = forms.BooleanField(required=False, initial=False, label="Restrita?", help_text='Somente participantes registrados podem acessar a votação.')
  help_email = forms.CharField(required=False, initial="", label="E-mail da votação", help_text='Endereço de e-mail utilizado para auxiliar os participantes.')
  
  if settings.ALLOW_ELECTION_INFO_URL:
    election_info_url = forms.CharField(required=False, initial="", label='Endereço de informações da votação', help_text='A URL deve ser informada por completo, por exemplo: "http://unidade.usp.br/informacao". Digite o endereço do site ou do documento PDF que contém informações extras da votação como, por exemplo, editais, candidatos e propostas.')
  
  # times
  voting_starts_at = SplitDateTimeField(label='Início da votação', help_text = 'Data e hora de início da votação',
                                   widget=SplitSelectDateTimeWidget, required=False)
  voting_ends_at = SplitDateTimeField(label='Fim da votação', help_text = 'Data e hora de fim da votação',
                                   widget=SplitSelectDateTimeWidget, required=False)

class ElectionTimeExtensionForm(forms.Form):
  voting_extended_until = SplitDateTimeField(label='Extender votação até', help_text = 'Data e hora para extetenter a votação.',
                                   widget=SplitSelectDateTimeWidget, required=False)

class ElectionDeleteConfirmForm(forms.Form):
  delete_confirm = forms.BooleanField(label="",help_text = 'Sim, eu confirmo a exclusão.')

class EmailVotersForm(forms.Form):
  subject = forms.CharField(label='Assunto', max_length=200)
  body = forms.CharField(label='Mensagem', max_length=4000, widget=forms.Textarea, required=False)
  send_to = forms.ChoiceField(label='Enviar para', initial='all', choices= [('all', 'Todos participantes'), ('voted', 'Apenas participantes que votaram'), ('not-voted', 'Apenas participantes que não votaram')])

class TallyNotificationEmailForm(forms.Form):
  subject = forms.CharField(label='Assunto', max_length=80)
  body = forms.CharField(label='Mensagem', max_length=2000, widget=forms.Textarea, required=False)
  send_to = forms.ChoiceField(label='Enviar para', choices= [('all', 'Todos participantes'), ('voted', 'Apenas participantes que votaram'), ('none', 'Ninguém -- Tem certeza sobre isso?')])

class VoterPasswordForm(forms.Form):
  voter_id = forms.CharField(label='Usuário', max_length=80)
  password = forms.CharField(label='Senha', widget=forms.PasswordInput(), max_length=100)

class VoterForm(forms.Form):
  voter_login_id = forms.CharField(label='Login (Identificador)', max_length=100)
  voter_email = forms.CharField(label='E-Mail', max_length=250)
  voter_name = forms.CharField(label='Nome', max_length=200)

class VoterFormEdit(VoterForm):
   voter_login_id_old = forms.CharField(widget=forms.HiddenInput())
   voter_password_new = forms.BooleanField(label='Gerar nova senha?', required=False, initial=False)
  