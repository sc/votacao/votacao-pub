# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('helios', '0005_election_delete'),
    ]

    operations = [
        migrations.AlterField(
            model_name='election',
            name='election_type',
            field=models.CharField(default=b'election', max_length=250, choices=[(b'election', b'Elei\xc3\xa7\xc3\xa3o'), (b'referendum', b'Referendo')]),
        ),
    ]
