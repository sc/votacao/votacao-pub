# -*- coding: utf-8 -*-

from view_utils import *
from security import *
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404

import user_forms

def require_admin(request):
  user = get_user(request)
  if not user or not user.admin_p:
    raise PermissionDenied()

  return user

def user_list(request):
  user = require_admin(request)
  
  page = int(request.GET.get('page', 1))
  limit = int(request.GET.get('limit', 10))
  qUser = request.GET.get('qUser','')

  obj = User.objects.filter(user_id__icontains = qUser)
  obj.all().order_by('user_id')
  obj_paginator = Paginator(obj, limit)
  obj_page = obj_paginator.page(page)
  total_itens = obj_paginator.count
  
  return render_template(request,'user_list', {'users':obj_page.object_list, 'obj_page': obj_page,
                                               'limit' : limit, 'total_itens': total_itens, 
                                               'qUser': qUser})


def user_new(request):
    user_login = require_admin(request)
    if not user_login or not user_login.admin_p:
        raise PermissionDenied()

    error = None
  
    if request.method == 'GET':
        form = user_forms.UserForm()
    else:
        check_csrf(request)
        form = user_forms.UserForm(request.POST)
        if form.is_valid():
            form.save();
            return HttpResponseRedirect(settings.SECURE_URL_HOST + reverse(user_list))
  
    form.setPasswordSuggestion()
    return render_template(request, "user_edit", {'user_form': form, 'error': error})


def user_edit(request,id):
    user_login = require_admin(request)
    if not user_login or not user_login.admin_p:
        raise PermissionDenied()

    error = None
    
    user = get_object_or_404(User, id=id)
    if request.method == 'GET':
        initial = {
            'user_id': user.user_id,
            'name': user.name,
            'email': user.info.get('email','')
            }
        form = user_forms.UserForm(initial)
    else:            
        check_csrf(request)
        form = user_forms.UserForm(request.POST)
        if form.is_valid():
            form.save(user)
            return HttpResponseRedirect(settings.SECURE_URL_HOST + reverse(user_list))
  
    return render_template(request, "user_edit", {'user_form': form, 'error': error})


def user_delete(request,id):
    user_login = require_admin(request)
    if not user_login or not user_login.admin_p:
        raise PermissionDenied()
    
    user = get_object_or_404(User, id=id)    
    if request.method == "GET":
        confirm_form = user_forms.UserDeleteConfirmForm()
        return render_template(request, 'user_delete',
                               {'user_del' : user, 
                                'confirm_form' : confirm_form})
    else:
        check_csrf(request)
        confirm_form = user_forms.UserDeleteConfirmForm(request.POST)    
        error = ""
    
        if confirm_form.is_valid():
            if user.user_id != request.POST['user_id_confirm']:
                error = "Entre com o nome do usuário"
            else:
                user.delete()
                return HttpResponseRedirect(settings.SECURE_URL_HOST + reverse(user_list))
            
        return render_template(request, 'user_delete',
                               {'user_del' : user, 
                                'confirm_form' : confirm_form,
                                'error' : error})
