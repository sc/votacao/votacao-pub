# -*- coding: utf-8 -*-

from django import forms
import random, json
from helios_auth.models import User

class UserForm(forms.Form):
    name = forms.CharField(label=u'Nome',required=True)
    user_id = forms.EmailField(label=u'Usuário',required=True)
    email = forms.EmailField(label=u'Email',required=True)
    password_new = forms.CharField(label=u'Senha',required=False,widget=forms.PasswordInput, help_text = u"Deixar vazio para manter a senha atual")


    def setPasswordSuggestion(self):
        self.fields['password_new'].help_text = u"Sugestão de senha: " + self.createPassword()


    #def clean_user_id(self):
    #    user_id = self.cleaned_data.get('user_id')
    #    if User.objects.filter(user_id=user_id):
    #        raise forms.ValidationError(u'Usuário já cadastrado.')
    #    return user_id


    def clean_password_new(self):
        password = self.cleaned_data.get('password_new')
        if len(password) > 0:
            if len(password) < 8:
                raise forms.ValidationError(u'Senha deve possuir no mínimo 8 caracteres.')
            if password.lower() == password:
                raise forms.ValidationError(u'Senha deve ter letras maiúsculas.')
            if password.upper() == password:
                raise forms.ValidationError(u'Senha deve ter letras minúsculas.')
            if not any(c in password for c in '0123456789'):
                raise forms.ValidationError(u'Senha deve conter números.')
            if not any(c in password for c in '@#$%&*+=;:.'):
                raise forms.ValidationError(u'Senha deve conter caracteres especiais.')
        return password

    
    def save(self, user=None):
        
        password = ''
        if user:
            password = user.info.get('password','')
        if self.cleaned_data['password_new'] != '':
            password = self.cleaned_data['password_new']
            
        params = {
            'user_id': self.cleaned_data['user_id'],
            'name': self.cleaned_data['name'],
            'info': json.dumps({'password': password, 'name': self.cleaned_data['name'],'email': self.cleaned_data['email']})
            }
        
        if user:          
            user.user_id = params['user_id']
            user.name = params['name']
            user.info = params['info']
            user.save()
        else:
            params['user_type'] = 'password'
            params['admin_p'] = False
            user = User.objects.create(**params)
        
        return user


    def createPassword(self):
        consoante_maiuscula = 'BCDFGJLMNPRSTXZ'
        consoante = 'bcdfgjmnprstxz'
        vogal = 'aeiou'
        simbolos = '@#$%&*+=;:.'
        numeral = '23456789'
        senha = ''
        senha += random.choice(consoante_maiuscula+consoante)
        senha += random.choice(vogal)
        senha += random.choice(consoante)
        senha += random.choice(vogal)
        senha += random.choice(consoante)
        senha += random.choice(vogal)
        senha += random.choice(simbolos+vogal)
        senha += random.choice(numeral)
        senha += random.choice(numeral)
        senha += random.choice(consoante_maiuscula+consoante)
        return senha


class UserDeleteConfirmForm(forms.Form):
    user_id_confirm = forms.CharField(label=u'Confirme o nome do usuário', max_length=100)
