# Helios Election System

Helios is an end-to-end verifiable voting system.

![Travis Build Status](https://travis-ci.org/benadida/helios-server.svg?branch=master)

[![Stories in Ready](https://badge.waffle.io/benadida/helios-server.png?label=ready&title=Ready)](https://waffle.io/benadida/helios-server)


# Adaptações do projeto original 

**Universidade de São Paulo (USP) - Superintendência de Tecnologia da Informação (STI)**

Várias adaptações foram realizadas, entre as principais podemos citar:

* Tradução para o português
* Conceito de votos brancos e nulos
* Interface para administração de usuários
* Interface para administrar eleitores
* Visualizar log de votações
* Troca de horário UTC por horário local
* Excluir eleições

# Instalação

* Esta instalação é baseada em SO Debian like
* Verifique se você possui a versão 2.7 do python

```
$ python --version
```

* Instale os pré-requisitos

```
$ sudo apt-get install virtualenv
$ sudo apt-get install git
$ sudo apt-get install postgresql
$ sudo apt-get install apache2
```

* Faça download do projeto

Entre com um usuário qualque do sistema e faça download do código fonte.
Vamos adotar o usuário "helios" e que vamos fazer download em "/home/helios".

```
$ git clone https://gitlab.uspdigital.usp.br/sc/votacao/votacao-pub.git
```

* Crie o ambiente virtual

No diretório raiz do projeto executar o comando

```
$ cd votacao
$ virtualenv --python=/usr/bin/python2.7 venv
$ source ./venv/bin/activate
$ pip install -r requirements.txt

```
Mantenha o ambiente virtual carregado sempre que for executar o manager.py do projeto.
Caso não esteja carregado, carregue-o através do seguinte comando.

```
$ source ./venv/bin/activate
```
OBS: O camando "**deactivate**" libera o ambiente virtual.

* Configuração do projeto

```
$ cp settings.dist.py settins.py
```

Realize as alterações necessárias neste arquivo


* Criar o banco de dados

```
$ sudo -u postgres createuser helios
$ sudo -u postgres createdb helios
```

* Criar estrutura de tabelas

```
$ python manage.py syncdb
$ python manage.py migrate
```

* Criar usuario administrador

Subistitua o e-mail e a senha pelos de sua preferência.

```
echo "from helios_auth.models import User; User.objects.create(user_type='password',user_id='admin@teste.com.br',name='Administrador do Sistema',info={'name':'Administrador do Sistema','password':'admin001','email':'votacao@teste.com.br'},admin_p=True)" | python manage.py shell > /dev/null
```

# Executar o servidor de desenvolvimento


Execute em um terminal


```
python manage.py runserver
```

Execute em outro terminal o gerenciador de tarefas

```
python manage.py celeryd
```

* Acessando o sistema


Abra o navegador e entre na url http://localhost:8000


# Instalação em ambiênte de produção

* Faça todos os passos da instalação


* Configure no arquivo settings.py DEBUG para False e ALLOWED_HOSTS para a sua URL


* Instale os pré-requisitos

```
$ sudo apt-get install libapache2-mod-wsgi
$ sudo apt-get install supervisor
```

* Configure um virtualhost no Apache HTTP

Creie um arquivo votacao.conf em /etc/apache2/sites-available contendo o seguinte conteúdo


```
<VirtualHost *:80>

    ServerName votacao.teste.com.br
    DocumentRoot /home/helios/votacao

    <Directory /home/helios/votacao>
        Require all granted
    </Directory>
    WSGIDaemonProcess grp-votacao python-home=/home/helios/votacao/venv python-path=/home/helios/votacao processes=10 threads=50
    WSGIProcessGroup grp-votacao
    WSGIScriptAlias / /home/helios/votacao/wsgi.py

</VirtualHost>
```

Ative e carrege as configurações


```
$ sudo a2ensite votacao.conf
$ sudo service apache2 reload 
```

* Configure o gerenciador de tarefas

Creie um arquivo votacao.conf em /etc/supervisor/conf.d/votacao.conf contendo o seguinte conteúdo

```
[program:votacao_celery_worker]
command=/home/helios/votacao/venv/bin/python manage.py celeryd
directory=/home/helios/votacao/
user=helios
autostart=true
autorestart=true
stdout_logfile=/var/log/votacao/celeryd.log
redirect_stderr=true
```

Crie o diretório /var/log/votacao

```
$ sudo mkdir /var/log/votacao
```

Reinicie o supervisor 

```
$ sudo service supervisor restart
```

Acesso sua URL pelo navegador.

**ATENÇÂO: Toda vez que atualizar o código fonte, reinicie o apache e o supervisor para as alterações terem efeito.**
